# TODO APP

TODO List is a todo list application implemented by VUE.JS and GOLANG. This project is client-side of the TODO List.

This project developed demo purpose of end-to-end TDD application with new technologies. 

- Vue.js for front-end 
- Pact for CDC test
- Jest for unit test
- Cypress for acceptance test

## Features
- Client API for adding and fetching TODO items

## Installation

Follow the Vue Js Guide for getting started building a project.
- Clone or download the repo
- npm to install dependencies
- npm run build serve to start front-end at localhost:8080
    or 
run docker image created 
    by 
docker build --tag vue-todo . 
    then 
docker run vue-todo

## Docker

By default, the Docker will expose port 8080, so change this within the Dockerfile if necessary. When ready, simply use the Dockerfile to build the image.

# Acceptance Test
Cypress is used to test user stories as test framework. It simulates the process of opening page, listing items and adding new item. REST API is stubbed and request is routed by Cypress.

    npm run test:cypress:open 

opens the browser application run by Cypress.
    then
runs tests
    npm run test:cypress

OR
- Tests can be run inside the browser application run by Cypress

# Unit and Component Test
Jest is used as unit and component test framework. 

- npm run test:jest

# CDC Test
Pact is used as a CDC Test framework. 

- npm run test:jest
also runs consumer test and it creates consumer-provider.json locally to use in provider test.

# GITLAB CI/CD
After push to remote repository gitlab job starts.


