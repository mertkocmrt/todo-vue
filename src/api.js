import axios from 'axios';
// import Task from './models/Task.js';

export class API {
  constructor(url) {
    if (url === undefined || url === "") {
      url = 'http://localhost:1234';
    }
    if (url.endsWith("/")) {
      url = url.substr(0, url.length - 1)
    }
    this.url = url
  }

  withPath(path) {
    if (!path.startsWith("/")) {
      path = "/" + path
    }
    return `${this.url}${path}`
  }

  async getAllTodo() {
    const headers = { "Content-Type": "application/json",  "Access-Control-Allow-Origin": "*"};
    return axios.get(this.withPath("/list"), { headers })
     .then(r => r.data);
  }

  async addTodo(task) {
    const headers = { "Content-Type": "application/json",  "Access-Control-Allow-Origin": "*"};
    return axios.post(this.withPath("/add"), task, { headers })
     .then(r => r.data);
  }
}

export default new API('http://localhost:1234');