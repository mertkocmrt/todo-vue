
describe('Opens Todo List Page', () => {

  it('and display Todo List', () => {
        cy.intercept('GET', 'http://localhost:1234/list', {
            statusCode: 200,
            body: {items:[{
              name: 'satori',
              id: 1,              
            }]}
          }).as('getTodo')

        cy.visit('/');
       
        cy.get('#app > :nth-child(1)').should('include.text', 'satori');
  })

  it('and add item', () => {
    cy.intercept('POST', 'http://localhost:1234/add', {
        statusCode: 200,
      }).as('addTodo')
    cy.visit('/');
    
    const input = cy.get('[data-cy=task]');
    var newTask = 'gaia';
    input.type(`${newTask}{enter}`);
    cy.get('#app > :nth-child(1)').should('include.text', 'gaia');
})
});