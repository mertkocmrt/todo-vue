import { shallowMount } from '@vue/test-utils';
// import TodoList from "../../src/components/TodoList.vue";
// import TodoItems from "../../src/components/TodoItems.vue";
// import {render, screen} from "@testing-library/vue";
import "@testing-library/jest-dom";
// import {API} from '../../src/api.js';
// import Task from '../../src/models/Task.js';
import Panel from "../../src/components/Panel.vue";


describe("Panel Layout", () => {
    const wrapper = shallowMount(Panel)
    it("has input", () => {
        const inputField  = wrapper.find('[data-testid="new-task"]')
        expect(inputField.element.outerHTML).toContain("New Task")
    });

    it("has button", () => {
        const button  = wrapper.find('[data-testid="add-button"]')
        expect(button.element.firstChild.textContent).toBe("ADD")
    });
});
describe('Unit Test', () => {

    describe('Add after press Enter', () => {
        const wrapper = shallowMount(Panel)
        let inputField;
        beforeEach(() => {
            wrapper.vm.newTask = "vue wei"
            inputField = wrapper.find('[data-testid="new-task"]')
        });
        it('allows for adding one todo item after press enter', async () => {
            inputField.trigger("keyup.enter");
            expect(wrapper.vm.newTask).toEqual("vue wei");
        })
    })

    describe('Add after click Button', () => {
        const wrapper = shallowMount(Panel)
        let addButton;
        beforeEach(() => {
            wrapper.vm.newTask = "vue wei"
            addButton = wrapper.find('[data-testid="add-button"]')
        });
        it('allows for adding one todo item after clicking button', async () => {
            addButton.trigger("click");
            expect(wrapper.vm.newTask).toEqual("vue wei");
        })
    })
})


