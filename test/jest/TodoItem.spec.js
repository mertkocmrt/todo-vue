import { mount } from '@vue/test-utils';
import TodoItem from "../../src/components/TodoItem.vue";
import "@testing-library/jest-dom";

describe("TodoItem Layout", () => {

      const wrapper = mount(TodoItem, {
        propsData: {
            task: {
                id:1,
                name:"vue wei"
            }
        }
      })
      
    it("has div", () => {
        const divField  = wrapper.find('[id="todo-item"]')
        expect(divField.element.outerHTML).toContain("todo-item")
    });
});
describe('Unit Test', () => {

    const wrapper = mount(TodoItem, {
        propsData: {
            task: {
                id:1,
                name:"vue wei"
            }
        }
      })
      
    it("has div", () => {
        const divField  = wrapper.find('[id="todo-item"]')
        expect(divField.element.firstChild.textContent).toBe("vue wei")
    });
})






// import TodoList from "../../src/components/TodoList.vue";
// import { shallowMount } from '@vue/test-utils';
// import TodoItems from "../../src/components/TodoItems.vue";
// import {render, screen} from "@testing-library/vue";
// import "@testing-library/jest-dom";
// import {API} from '../../src/api.js';
// import Task from '../../src/models/Task.js';


// describe("Todo List Layout", () => {

//     // it("has title", () =>{
//     //     const wrapper = shallowMount(TodoList, {
//     //         propsData: {
//     //           title: 'TODO LIST'
//     //         }
//     //       });
//     //     expect(wrapper.text()).toMatch('TODO LIST')
//     // });

//     // it("has list", () => {
//     //     const { queryByTestId } = render(TodoItems);
//     //     queryByTestId('list');
//     // });

//     // it("has task input and button", () => {
//     //     const {container} = render(TodoItems);
//     //     const input = screen.queryByPlaceholderText("New Task");
//     //     expect(input).toBeInTheDocument();
//     //     const button = container.querySelector('button');
//     //     expect(button).toBeInTheDocument();
//     // });
// });
// describe('ADDING', () => {
//     // describe('Add after press Enter', () => {
//     //     const wrapper = shallowMount(TodoItems)
//     //     let inputField;
//     //     beforeEach(() => {
//     //         wrapper.vm.newTask = "vue wei"
//     //         inputField = wrapper.find('[data-testid="task"]')
//     //         inputField.trigger("createTask")
//     //     });
//     //     it('allows for adding one todo item after press enter', async () => {
//     //         inputField.trigger("keyup.enter");
//     //         expect(wrapper.vm.tasks).toEqual(["vue wei"]);
//     //     })
//     // })

//     // describe('Add after click Button', () => {
//     //     const wrapper = shallowMount(TodoItems)
//     //     let addButton;
//     //     beforeEach(() => {
//     //         wrapper.vm.newTask = "vue wei"
//     //         addButton = wrapper.find('[data-testid="add"]')
//     //     });
//     //     it('allows for adding one todo item after clicking button', async () => {
//     //         addButton.trigger("click");
//     //         expect(wrapper.vm.tasks).toEqual(["vue wei"]);
//     //     })
//     // })
// })
// describe('SHOW DATA', () => {
//     // describe('show data when created', () => {
//     //     const wrapper = shallowMount(TodoItems)
//     //     let listField;
//     //     beforeEach(() => {
//     //         wrapper.vm.tasks =[ 
//     //             new Task("vue wei",9999)
//     //         ]
//     //     });
//     //     it('allows for adding one todo item after press enter', async () => {
//     //         listField = wrapper.find('[data-testid="list"]')
//     //         expect(listField.element.firstChild.textContent).toEqual("vue wei");
//     //     })
//     // })

//     // describe('Add after click Button', () => {
//     //     const wrapper = shallowMount(TodoItems)
//     //     let addButton;
//     //     beforeEach(() => {
//     //         wrapper.vm.newTask = "vue wei"
//     //         addButton = wrapper.find('[data-testid="add"]')
//     //     });
//     //     it('allows for adding one todo item after clicking button', async () => {
//     //         addButton.trigger("click");
//     //         expect(wrapper.vm.tasks).toEqual(["vue wei"]);
//     //     })
//     // })
// })

