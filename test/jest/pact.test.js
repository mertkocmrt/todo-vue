// import TodoList from "../../src/components/TodoList.vue";
// import { shallowMount } from '@vue/test-utils';
// import TodoItems from "../../src/components/TodoItems.vue";
// import {render, screen} from "@testing-library/vue";
import "@testing-library/jest-dom";
import {API} from '../../src/api.js';
import Task from '../../src/models/Task.js';
import { resolve } from 'path';
import { Pact } from '@pact-foundation/pact';

const EXPECTED_BODY = {"items":[{"id":1,"name":""}]};
const EXPECTED_NAME = {"id":1,"name":""};
const port = 9393;
const provider = new Pact({
    cors: true,
    port: port,
    log: resolve(process.cwd(), 'logs', 'pact.log'),
    loglevel: 'debug',
    dir: resolve(process.cwd(), 'pacts'),
    spec: 2,
    pactfileWriteMode: 'update',
    consumer: 'consumer',
    provider: 'provider',
    host: 'localhost'
});

describe("Todo List Layout", () => {

    // it("has title", () =>{
    //     const wrapper = shallowMount(TodoList, {
    //         propsData: {
    //           title: 'TODO LIST'
    //         }
    //       });
    //     expect(wrapper.text()).toMatch('TODO LIST')
    // });

    // it("has list", () => {
    //     const { queryByTestId } = render(TodoItems);
    //     queryByTestId('list');
    // });

    // it("has task input and button", () => {
    //     const {container} = render(TodoItems);
    //     const input = screen.queryByPlaceholderText("New Task");
    //     expect(input).toBeInTheDocument();
    //     const button = container.querySelector('button');
    //     expect(button).toBeInTheDocument();
    // });
});
describe('ADDING', () => {
    // describe('Add after press Enter', () => {
    //     const wrapper = shallowMount(TodoItems)
    //     let inputField;
    //     beforeEach(() => {
    //         wrapper.vm.newTask = "vue wei"
    //         inputField = wrapper.find('[data-testid="task"]')
    //         inputField.trigger("createTask")
    //     });
    //     it('allows for adding one todo item after press enter', async () => {
    //         inputField.trigger("keyup.enter");
    //         expect(wrapper.vm.tasks).toEqual(["vue wei"]);
    //     })
    // })

    // describe('Add after click Button', () => {
    //     const wrapper = shallowMount(TodoItems)
    //     let addButton;
    //     beforeEach(() => {
    //         wrapper.vm.newTask = "vue wei"
    //         addButton = wrapper.find('[data-testid="add"]')
    //     });
    //     it('allows for adding one todo item after clicking button', async () => {
    //         addButton.trigger("click");
    //         expect(wrapper.vm.tasks).toEqual(["vue wei"]);
    //     })
    // })
})
describe('SHOW DATA', () => {
    // describe('show data when created', () => {
    //     const wrapper = shallowMount(TodoItems)
    //     let listField;
    //     beforeEach(() => {
    //         wrapper.vm.tasks =[ 
    //             new Task("vue wei",9999)
    //         ]
    //     });
    //     it('allows for adding one todo item after press enter', async () => {
    //         listField = wrapper.find('[data-testid="list"]')
    //         expect(listField.element.firstChild.textContent).toEqual("vue wei");
    //     })
    // })

    // describe('Add after click Button', () => {
    //     const wrapper = shallowMount(TodoItems)
    //     let addButton;
    //     beforeEach(() => {
    //         wrapper.vm.newTask = "vue wei"
    //         addButton = wrapper.find('[data-testid="add"]')
    //     });
    //     it('allows for adding one todo item after clicking button', async () => {
    //         addButton.trigger("click");
    //         expect(wrapper.vm.tasks).toEqual(["vue wei"]);
    //     })
    // })
})

describe('API Pact test', () => {
    beforeAll(() => provider.setup());
    afterEach(() => provider.verify());
    afterAll(() => provider.finalize());

    describe("Todo service", () => {
        describe("When doing a call to add a todo", () => {
            describe('adding a todo', () => {
                test('todo added', async () => {
                    provider.addInteraction({
                        state: 'todo added',
                        uponReceiving: 'a request to add a todo',
                        withRequest: {
                        method: 'POST',
                        path: '/add',
                        },
                        willRespondWith: {
                        status: 200,
                        headers: {
                            "Content-Type": "application/json; charset=utf-8",
                        },
                        body: EXPECTED_NAME,
                        },
                    });
                    
                    const api = new API(provider.mockService.baseUrl);
                    const task = await api.addTodo(new Task("",1));   
                    expect(task).toMatchObject<Task>({id:Number, name:String})
                });
            });
        });
        describe("When doing a call to fetch all todo", () => {
            describe('retrieving all todo', () => {
                test('todo exist', async () => {           
                    provider.addInteraction({
                        state: 'todo exist',
                        uponReceiving: 'a request to get all todo',
                        withRequest: {
                        method: 'GET',
                        path: '/list',
                        },
                        willRespondWith: {
                        status: 200,
                        headers: {
                            "Content-Type": "application/json; charset=utf-8",
                        },
                        body: EXPECTED_BODY,
                        },
                    });
                    
                    const api = new API(provider.mockService.baseUrl);
                    const todo = await api.getAllTodo();   
                    expect(todo).toStrictEqual(EXPECTED_BODY);
                });
            });
        })
    });
});


